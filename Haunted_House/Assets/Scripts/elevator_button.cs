﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class elevator_button : MonoBehaviour
{


    public Transform up_lever;
    public Transform down_lever;
    private void Update()
    {
        if (Vector3.Distance(transform.position, up_lever.position) >
            Vector3.Distance(transform.position, down_lever.position))
        {
            CommonClass.elevator_up = true;
        }
    }

  
}
