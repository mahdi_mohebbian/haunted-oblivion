﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flicker : MonoBehaviour
{
    Light my_light;
    // Start is called before the first frame update
    void Start()
    {
        my_light = gameObject.GetComponent<Light>();
        StartCoroutine (flicker_light(0.1f,my_light));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static IEnumerator flicker_light(float seconds,Light my_light)
    {
        while(true){
            // receive information about the flickering or not flickering right here

            // Debug.Log("************--------- flicker 3");
            // yield return new WaitForSeconds(Random.Range(0.05f,.5f));
            // my_light.enabled = true;
            // yield return new WaitForSeconds(Random.Range(0.05f,10.5f));
            // my_light.enabled = false;
            // //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            if (CommonClass.flicker_level == 0){
                my_light.enabled = true;
            }

            if (CommonClass.flicker_level == 1){
                 Debug.Log("************--------- flicker 1");
                yield return new WaitForSeconds(Random.Range(0.05f,0.5f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(20.0f,25.0f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }



            if (CommonClass.flicker_level == 2){
                Debug.Log("************--------- flicker 2");
                yield return new WaitForSeconds(Random.Range(0.05f,0.5f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(15.0f,20.0f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }



            if (CommonClass.flicker_level == 3){
                Debug.Log("************--------- flicker 3");
                yield return new WaitForSeconds(Random.Range(0.01f,0.05f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(10.0f,15.0f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }


            if (CommonClass.flicker_level == 4){
                Debug.Log("************--------- flicker 4");
                yield return new WaitForSeconds(Random.Range(0.01f,0.05f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(5.0f,10.0f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }



            if (CommonClass.flicker_level == 5){
                Debug.Log("************--------- flicker 5");
                yield return new WaitForSeconds(Random.Range(0.01f,0.05f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(1.0f,5.0f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }


            if (CommonClass.flicker_level == 6){
                Debug.Log("************--------- flicker 6");
                yield return new WaitForSeconds(Random.Range(0.01f,0.05f));
            my_light.enabled = true;
            yield return new WaitForSeconds(Random.Range(0.01f,0.05f));
            my_light.enabled = false;
            //yield return new WaitForSeconds(Random.Range(1.7f,1.9f));
            }
            
            yield return new WaitForSeconds(0.1f);
            
        }
    }
}
