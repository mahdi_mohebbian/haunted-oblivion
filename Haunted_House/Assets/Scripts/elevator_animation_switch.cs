﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevator_animation_switch : MonoBehaviour
{
    private bool start=false;
    private Vector3 target0;
    private Vector3 target1;

    public Transform elevator;
    // Start is called before the first frame update
    void Start()
    {
        target0 = new Vector3(elevator.position.x,(float)(elevator.position.y-4.3),elevator.position.z);
        target1 = elevator.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (CommonClass.elevator_down == true)
        {
            float step = 2 * Time.deltaTime;
            elevator.position = Vector3.MoveTowards(elevator.position,target0 , step);
            Debug.Log((Mathf.Abs(elevator.position.y-target0.y)).ToString());
            if (Mathf.Abs(elevator.position.y-target0.y)<=0.01)
            {
                Debug.Log("stoped ------------------");
                CommonClass.elevator_down = false;
            }
        }
        
        if (CommonClass.elevator_up == true)
        {
            float step = 2 * Time.deltaTime;
            elevator.position = Vector3.MoveTowards(elevator.position,target1 , step);
            Debug.Log((Mathf.Abs(elevator.position.y-target1.y)).ToString());
            if (Mathf.Abs(elevator.position.y-target1.y)<=0.01)
            {
                Debug.Log("stoped ------------------");
                CommonClass.elevator_up = false;
            }
        }
        
        if (Input.GetKeyDown("space"))
        {
           start = true;
        }
        

    }
}
