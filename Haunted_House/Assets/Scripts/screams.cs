﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class screams : MonoBehaviour
{
    public AudioSource[] audios;
    private float seconds_passed = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        seconds_passed += Time.deltaTime;
        if (seconds_passed > 15)
        {
            seconds_passed = 0.0f;
            int chance = Random.Range(1,100);

            if (chance < 35)
            {
                Random rand = new Random();
                int index = Random.Range(0, audios.Length);
                audios[index].Play();
            }
        }
    }
}
