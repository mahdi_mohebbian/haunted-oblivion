﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class demon : MonoBehaviour
{
    private bool in_motion = false;
    private Vector3 start;
    private Vector3 end;
    private float time_before_restart = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (CommonClass.death_call == true){
            GameObject.Find("demon_mesh1").GetComponent<Renderer>().enabled = true;
            GameObject.Find("demon_mesh2").GetComponent<Renderer>().enabled = true;
            GameObject.Find("demon_mesh3").GetComponent<Renderer>().enabled = true;
            GameObject.Find("demon_mesh4").GetComponent<Renderer>().enabled = true;
            if (CommonClass.player_dead == false){
                if (in_motion){
                    float step = 10 * Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position,end , step);
                    if (transform.position == end){
                        in_motion = false;
                    }
                }
                else
                {
                    if (CommonClass.area == "r3"){
                        GameObject[] starts = GameObject.FindGameObjectsWithTag("seg3_a");
                        int index = (Random.Range(0,starts.Length));
                        start = GameObject.Find(starts[index].name).transform.position;
                        end = GameObject.Find(starts[index].name.Substring(0,starts[index].name.Length-1)+"b").transform.position;

                    }
                    if (CommonClass.area == "r2"){
                        GameObject[] starts = GameObject.FindGameObjectsWithTag("seg2_a");
                        int index = (Random.Range(0,starts.Length));
                        start = GameObject.Find(starts[index].name).transform.position;
                        end = GameObject.Find(starts[index].name.Substring(0,starts[index].name.Length-1)+"b").transform.position;
                    }
                    if (CommonClass.area == "r1"){
                        GameObject[] starts = GameObject.FindGameObjectsWithTag("seg1_a");
                        int index = (Random.Range(0,starts.Length));
                        start = GameObject.Find(starts[index].name).transform.position;
                        end = GameObject.Find(starts[index].name.Substring(0,starts[index].name.Length-1)+"b").transform.position;
                    }
                    transform.position = start;
                    in_motion = true;
                }

            }
            else{
                GameObject des = GameObject.Find("Main Camera");
                gameObject.transform.position = des.transform.position+des.transform.forward*(2);
                gameObject.transform.LookAt(des.transform.position);
                time_before_restart += Time.deltaTime;
                if (time_before_restart >= 10.0f)
                {
                    SceneManager.LoadScene("lose");
                }
            }

        }
        else {
            GameObject.Find("demon_mesh1").GetComponent<Renderer>().enabled = false;
            GameObject.Find("demon_mesh2").GetComponent<Renderer>().enabled = false;
            GameObject.Find("demon_mesh3").GetComponent<Renderer>().enabled = false;
            GameObject.Find("demon_mesh4").GetComponent<Renderer>().enabled = false;
        }
        
    }
}
