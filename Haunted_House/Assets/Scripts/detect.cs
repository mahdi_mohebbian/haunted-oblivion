﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class detect : MonoBehaviour
{
    private bool once = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (CommonClass.mom_item_found == 3 && CommonClass.girl_item_found == 2 && once == false)
        {
            once = true;
            CommonClass.player_won = true;
            CommonClass.elevator_down = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.name == "demon"){
            if (CommonClass.death_call == true){
                CommonClass.player_dead = true;
                //Application.Quit();
            }
             
        }
        if (other.gameObject.name == "Exit")
        {
            SceneManager.LoadScene("forgive");

        }
    }
}
