﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cry : MonoBehaviour
{
    private AudioSource audioData;

    private float seconds_passed1 = 0.0f;
    private float seconds_passed2 = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();
//        if (gameObject.name == "cry1")
//        {
//            StartCoroutine(CommonClass.waitSeconds(10,audioData));
//            StartCoroutine(CommonClass.waitSeconds(80,audioData));
//        }
//
//        if (gameObject.name == "cry2")
//        {
//            StartCoroutine(CommonClass.waitSeconds(30,audioData));
//             StartCoroutine(CommonClass.waitSeconds(115,audioData));
//        }
        
    }

    // Update is called once per frame
    void Update()
    {
        seconds_passed1 += Time.deltaTime;
        seconds_passed2 += Time.deltaTime;
        if (gameObject.name == "cry1")
        {
            if (seconds_passed1 > 15.0f)
            {
                seconds_passed1 = 0.0f;
                int chance = Random.Range(1,100);

                if (chance < 15)
                {
                    audioData.Play();
                }
                
            }
        }
        else
        {
            if (seconds_passed2 > 20.0f)
            {
                seconds_passed2 = 0.0f;
                int chance = Random.Range(1,100);

                if (chance < 15)
                {
                    audioData.Play();
                }
                
            }
        }
    }

    
}
