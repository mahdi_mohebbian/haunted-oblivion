﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thunders : MonoBehaviour
{
    
    private AudioSource audioData;
    // Start is called before the first frame update
    void Start()
    {
         audioData = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        int chance = Random.Range(1,100000);
        
        if (gameObject.name=="thunder1" && chance < 10)
        {
            audioData.Play();
            Debug.Log("**********"+"thunder1"+chance.ToString());
        }

        if (gameObject.name=="thunder2" && chance < 20 && chance>10)
        {
            audioData.Play();
            Debug.Log("**********"+"thunder2"+chance.ToString());
        }

        if (gameObject.name=="thunder3" && chance < 30 && chance>20)
        {
            audioData.Play();
            Debug.Log("**********"+"thunder3"+chance.ToString());
        }
        
    }
}
