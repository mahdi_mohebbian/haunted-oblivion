﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class area_collider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        
        // first we check if the area is new
        
        if (gameObject.name != CommonClass.area && other.gameObject.name=="User_area")
        {
            
            if (gameObject.name == "r1" && CommonClass.death_call == false){
                Debug.Log("++++++++++++++++++ IN SEG 1");
                
                CommonClass.under_attack = true;
                if(CommonClass.death_call != true)
                    if (CommonClass.flicker_level ==0)
                        CommonClass.flicker_level = 1;
            }

            else if(gameObject.name== "r2" && CommonClass.death_call == false && CommonClass.been_in_r2 == false)
            {
                Debug.Log("++++++++++++++++++ IN SEG 2");
                CommonClass.been_in_r2 = true;
                CommonClass.seconds_left -= 35;
                if (CommonClass.seconds_left <= 0){
                    CommonClass.seconds_left = 0;
                    CommonClass.death_call = true;
                    
                }
            }

            else if(gameObject.name == "r3" && CommonClass.death_call == false && CommonClass.been_in_r3 == false)
            {
                CommonClass.been_in_r2 = true;
                Debug.Log("++++++++++++++++++ IN SEG 3");
                CommonClass.seconds_left -= 50;
                if (CommonClass.seconds_left <= 0){
                    CommonClass.seconds_left = 0;
                    CommonClass.death_call = true;
                }
            }

            else if(gameObject.name == "r0")
            {
                Debug.Log("++++++++++++++++++ IN SEG 0");
                
                CommonClass.seconds_left = 180.0f;
                CommonClass.under_attack = false;
                CommonClass.death_call = false;
                CommonClass.been_in_r2 = false;
                CommonClass.been_in_r2 = false;
                CommonClass.player_dead = false;
                CommonClass.flicker_level = 0;
            }

            
        }
        CommonClass.area = gameObject.name;
        
    }
}
