﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scene0_count_down : MonoBehaviour
{
    private float time = 60.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if ( time < 0 )
        {
            SceneManager.LoadScene("SampleScene");
        }

        Debug.Log(time);
    }
}
