﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


static class CommonClass
{
    public static string area="r0";
    public static bool under_attack = false;
    public static float seconds_left = 180.0f;
    public static bool death_call = false;
    public static bool been_in_r2 = false;
    public static bool been_in_r3 = false;
    public static int flicker_level = 0 ;

    public static int mom_item_found = 0;
    public static int girl_item_found = 0;
    
    public static bool player_dead = false;
    public static bool player_won = false;
    public static bool elevator_down = false;
    public static bool elevator_up = false;

    public static IEnumerator waitSeconds(int seconds,AudioSource audio)
    {
        yield return new WaitForSeconds(seconds);
        audio.Play();
        
    }

}

