﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background_music_manager : MonoBehaviour
{
    public static AudioSource background_music_normal;
    public static AudioSource background_music_panic;

    private bool last_stat = false;
    private AudioSource demon_sound;
    // Start is called before the first frame update
    void Start()
    {
        demon_sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("DEMONNNNNNNN 1");
        if (CommonClass.death_call == true)
        {
            if (last_stat == false)
            {
                Debug.Log("DEMONNNNNNNN 2");
                demon_sound.Play();
                last_stat = CommonClass.death_call;
            }
        }
        else
        {
            if (last_stat == true)
            {
                Debug.Log("DEMONNNNNNNN 3");
                demon_sound.Pause();
                last_stat = CommonClass.death_call;
            }
        }
    }
}
