﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class box_management : MonoBehaviour
{
    private string name;
    private List<string> mom_item = new List<string>(){"ring_box","Aniversary_gift","boy_name" };
    private List<string> girl_item = new List<string>() {"origami","bear"};
//    private string[] mom_items = {"ring_box","Aniversary_gift","boy_name" };
//    private string[] girl_item = {"origami","bear"};
    
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name == "box_girl")
        {
            name = "girl";
        }
        else
        {
            name = "mom";
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (name == "girl")
        {
            if (girl_item.Contains(other.gameObject.name))
            {
                girl_item.Remove(other.gameObject.name);
                CommonClass.girl_item_found += 1;
                Debug.Log("----------------------------");
                Debug.Log(CommonClass.girl_item_found);
            }
        }

        if (name == "mom")
        {
            if (mom_item.Contains(other.gameObject.name))
            {
                mom_item.Remove(other.gameObject.name);
                CommonClass.mom_item_found += 1;
                Debug.Log("----------------------------");
                Debug.Log(CommonClass.mom_item_found);
            }
        }
        
    }
}
